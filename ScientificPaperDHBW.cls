\ProvidesClass{ScientificPaperDHBW} [2017/09/21 v1.00 Scientific Paper for the DHBW Horb]
\NeedsTeXFormat{LaTeX2e}

%%%%%%%%%%%%% Option Commands %%%%%%%%%%%%%%
\newcommand{\headStyle}{\pagestyle{plain}}
\newcommand{\headNumbering}{\pagenumbering{Roman}}
\newcommand{\textStyle}{\pagestyle{headings}}
\newcommand{\textNumbering}{\pagenumbering{arabic}}
%%%%%%%%%%%%% Options %%%%%%%%%%%%%%
\DeclareOption{headStyle=plain}{\renewcommand{\headStyle}{\pagestyle{plain}}}
\DeclareOption{headStyle=headings}{\renewcommand{\headStyle}{\pagestyle{headings}}}
\DeclareOption{headStyle=empty}{\renewcommand{\headStyle}{\pagestyle{empty}}}
\DeclareOption{headNumbering=Roman}{\renewcommand{\headNumbering}{\pagenumbering{Roman}}}
\DeclareOption{headNumbering=arabic}{\renewcommand{\headNumbering}{\pagenumbering{arabic}}}
\DeclareOption{textStyle=headings}{\renewcommand{\textStyle}{\pagestyle{headings}}}
\DeclareOption{textStyle=plain}{\renewcommand{\textStyle}{\pagestyle{plain}}}
\DeclareOption{textStyle=empty}{\renewcommand{\textStyle}{\pagestyle{empty}}}
\DeclareOption{textNumbering=arabic}{\renewcommand{\textNumbering}{\pagenumbering{arabic}}}
\DeclareOption{textNumbering=Roman}{\renewcommand{\textNumbering}{\pagenumbering{Roman}}}

\DeclareOption*{\InputIfFileExists{\CurrentOption.min}{}{%
	\PassOptionsToClass{\CurrentOption}{scrbook}}}
% --- Class structure: execution of options part
% ---
\ProcessOptions \relax
% --- Class structure: declaration of options part
% ---
\LoadClass[oneside, headsepline, footsepline, toc=bibliography]{scrbook}
%\LoadClass[%
%	pdftex,
%	oneside,			% Zweiseitiger Druck.
%	11pt,				% Schriftgroesse
%	parskip=half,		% Halbe Zeile Abstand zwischen Absätzen.
%	topmargin = 10pt,	% Abstand Seitenrand (Std:1in) zu Kopfzeile [laut log: unused]
%	headheight = 12pt,	% Höhe der Kopfzeile
%	headsep = 30pt,	% Abstand zwischen Kopfzeile und Text Body  [laut log: unused]
%	headsepline,		% Linie nach Kopfzeile.
%	footsepline,		% Linie vor Fusszeile.
%	footheight = 16pt,	% Höhe der Fusszeile
%	abstracton,		% Abstract Überschriften
%	DIV=calc,		% Satzspiegel berechnen
%	BCOR=8mm,		% Bindekorrektur links: 8mm
%	headinclude=false,	% Kopfzeile nicht in den Satzspiegel einbeziehen
%	footinclude=false,	% Fußzeile nicht in den Satzspiegel einbeziehen
%	listof=totoc,		% Abbildungs-/ Tabellenverzeichnis im Inhaltsverzeichnis darstellen
%	toc=bibliography,	% Literaturverzeichnis im Inhaltsverzeichnis darstellen
%]{scrbook}

%%%%%%%%%% Packeges %%%%%%%%%%
%Page layout
\RequirePackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm,includeheadfoot]{geometry}
%Header and footer
\RequirePackage{longtable}
\RequirePackage{graphicx}
\RequirePackage[utf8]{inputenc}
\RequirePackage{times}
\RequirePackage{ifthen}
\RequirePackage{setspace}
\RequirePackage{csquotes}
\RequirePackage[backend=biber,style=alphabetic,]{biblatex}
%\setcounter{biburllcpenalty}{1}
%%%%%%%%%% Booleans %%%%%%%
\newboolean{alreadyEnded}
%%%%%%%%%% Data %%%%%%%%%%
%Autor
\def\autor#1{\def\@autor{#1}}
\newcommand{\Autor}{\@autor}
%Firma
\def\firma#1{\def\@firma{#1}}
\newcommand{\Firma}{\@firma}
%FirmenOrt
\def\firmenort#1{\def\@firmenort{#1}}
\newcommand{\Firmenort}{\@firmenort}
%AbgabeOrt
\def\abgabeort#1{\def\@abgabeort{#1}}
\newcommand{\Abgabeort}{\@abgabeort}
%Abschluss
\def\abschluss#1{\def\@abschluss{#1}}
\newcommand{\Abschluss}{\@abschluss}
%Title
\def\title#1{\def\@title{#1}}
\newcommand{\Title}{\@title}
%Kurs
\def\kurs#1{\def\@kurs{#1}}
\newcommand{\Kurs}{\@kurs}
%Studiengang
\def\studiengang#1{\def\@studiengang{#1}}
\newcommand{\Studiengang}{\@studiengang}
%Hochschule
\def\hochschule#1{\def\@hochschule{#1}}
\newcommand{\Hochschule}{\@hochschule}
%Betreuer
\def\betreuer#1{\def\@betreuer{#1}}
\newcommand{\Betreuer}{\@betreuer}
%BetreuerAbschluss
\def\betreuerabschluss#1{\def\@betreuerabschluss{#1}}
\newcommand{\Betreuerabschluss}{\@betreuerabschluss}
%Zeitraum
\def\zeitraum#1{\def\@zeitraum{#1}}
\newcommand{\Zeitraum}{\@zeitraum}
%Arbeit
\def\arbeit#1{\def\@arbeit{#1}}
\newcommand{\Arbeit}{\@arbeit}
%MartrikelNR
\def\matNR#1{\def\@matNR{#1}}
\newcommand{\MatNR}{\@matNR}
%AbgabeDatum
\def\abgabeDatum#1{\def\@abgabeDatum{#1}}
\newcommand{\AbgabeDatum}{\@abgabeDatum}

%%%%%%%%%% New Commands %%%%%%%%%%
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
%%%%%%%%%% Title %%%%%%%%%%
\renewcommand{\maketitle}{
	\begin{titlepage}
		\center
		%----------------------------------------------------------------------------------------
		%   Logos
		%----------------------------------------------------------------------------------------

		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \large
				\includegraphics[width = \textwidth]{images/FIT_Logo.jpg}
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \large
				\includegraphics[width = \textwidth]{images/hochschule.png}
			\end{flushright}
		\end{minipage}\\[1cm]

		%----------------------------------------------------------------------------------------
		%   TITLE SECTION
		%----------------------------------------------------------------------------------------

		\HRule \\[0.4cm]
		{ \LARGE \bfseries \Title{} }\\[0.4cm] % Title of your document
		\HRule \\[1cm]

		%----------------------------------------------------------------------------------------
		%   HEADING SECTIONS
		%----------------------------------------------------------------------------------------

		\LARGE \Arbeit\\ \vspace{2cm} % Name of your university/college
		%\Large \Abschluss\\ \vspace{1cm} % Major heading such as course name
		\large des Studiengangs \Studiengang\\  % Minor heading such as course title
		\large an der \Hochschule\\ \vspace{2cm}
		\large von \Autor\\ 
		\vfill
		\large \AbgabeDatum\\[2cm] % Date, change the \today to a set date if you want to be precise

		%----------------------------------------------------------------------------------------
		%   AUTHOR SECTION
		%----------------------------------------------------------------------------------------
		\begin{spacing}{1.2}
			\begin{tabbing}
				mmmmmmmmmmmmmmmmmmm             \= \kill
				\textbf{Bearbeitungszeitraum:}       \>  \Zeitraum\\
				\textbf{Matrikelnummer, Kurs}  \>  \MatNR{}, \Kurs\\
				\textbf{Ausbildungsfirma}                  \>  \Firma, \Firmenort\\
				\textbf{Betreuer}               \>  \Betreuerabschluss  \Betreuer\\
			\end{tabbing}
		\end{spacing}
		
%		\begin{minipage}{0.4\textwidth}
%			\begin{flushleft} \large
%				\emph{Autor:}\\
%				\Autor \\
%				\vspace{0.5cm}
%				\emph{Matrikelnummer, Kurs:}\\
%				\MatNR{}, \Kurs \\
%				%\vspace{\baselineskip}
%				\vspace{0.5cm}
%				\emph{Bearbeitungszeitraum:}\\
%				\Zeitraum\\
%				%\vspace{\baselineskip}
%				%\vspace{\baselineskip}
%			\end{flushleft}
%		\end{minipage}
%		~
%		\begin{minipage}{0.4\textwidth}
%			\begin{flushright} \large
%				\emph{Ausbildungsfirma:}\\
%				\Firma, \Firmenort \\
%				\vspace{0.5cm}
%				\emph{Betreuer:} \\
%				\Betreuer, \\ \Betreuerabschluss \\
%			\end{flushright}
%		\end{minipage}\\[1cm]

		%----------------------------------------------------------------------------------------
		%   DATE SECTION
		%----------------------------------------------------------------------------------------

		
		%\vfill % Fill the rest of the page with whitespace
	\end{titlepage}
	\clearpage
	\headStyle
	%\headNumbering
}

%%%%%%%%%% Erklärung %%%%%%%%%%
\newcommand{\headoptions}[2]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{#2}}{}
	\ifthenelse{\equal{#1}{toc,empty}}{\addchap{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{notoc,empty}}{\addchap*{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{empty,toc}}{\addchap{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{empty,notoc}}{\addchap*{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{notoc}}{\addchap*{#2}}{}
	\ifthenelse{\equal{#1}{empty}}{\addchap*{#2} \thispagestyle{empty}}{}

}
\newcommand{\erklaerungDE}[1][notoc]{
	\headoptions{#1}{Erklärung}

	%\section*{Erklärung}
	\vspace*{2em}

	Ich versichere hiermit, dass ich meine Projektarbeit \Arbeit{} mit dem
	Thema: \textit{\Title} selbstständig verfasst und keine anderen als die angegebenen Quellen und Hilfsmittel
	benutzt habe.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
\newcommand{\erklaerungEN}[1][notoc]{
	\headoptions{#1}{Declaration of Authorship}
	%\section*{Declaration of Authorship }
	\vspace*{2em}
	Hereby I solemnly declare:
	\begin{enumerate}
		\item that this \Arbeit{}, titled \textit{\Title} is entirely the product of my own scholarly work, unless otherwise indicated in the text or references, or acknowledged below;
		\item I have indicated the thoughts adopted directly or indirectly from other sources at the appropriate places within the document;
		\item this \Arbeit{} has not been submitted either in whole or part, for a degree at this or any other university or institution;
		\item I have not published this \Arbeit{} in the past;
		\item the printed version is equivalent to the submitted electronic one.
	\end{enumerate}
	\vspace{3em}
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
%%%%%%%%%% Sperrvermerk %%%%%%%%%%
\newcommand{\sperrvermerkDE}[1][notoc]{
	\headoptions{#1}{Sperrvermerk}
	%\section*{Sperrvermerk}
	\vspace*{2em}
	Die vorliegende {\Arbeit{}} mit dem Titel {\itshape{} \Title{}\/} enthält unternehmensinterne bzw. vertrauliche Informationen der {\Firma}, ist deshalb mit einem Sperrvermerk versehen und wird ausschließlich zu Prüfungszwecken am Studiengang {\Studiengang} der \Hochschule vorgelegt. Sie ist ausschließlich zur Einsicht durch den zugeteilten Gutachter, die Leitung des Studiengangs und ggf. den Prüfungsausschuss des Studiengangs bestimmt.  Es ist untersagt,
	\begin{itemize}
		\item den Inhalt dieser Arbeit (einschließlich Daten, Abbildungen, Tabellen, Zeichnungen usw.) als Ganzes oder auszugsweise weiterzugeben,
		\item Kopien oder Abschriften dieser Arbeit (einschließlich Daten, Abbildungen, Tabellen, Zeichnungen usw.) als Ganzes oder in Auszügen anzufertigen,
		\item diese Arbeit zu veröffentlichen bzw. digital, elektronisch oder virtuell zur Verfügung zu stellen.
	\end{itemize}
	Jede anderweitige Einsichtnahme und Veröffentlichung – auch von Teilen der Arbeit – bedarf der vorherigen Zustimmung durch den Verfasser und {\Firma}.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
\newcommand{\sperrvermerkEN}[1][notoc]{
	\headoptions{#1}{Confidentiality clause}

	\ifthenelse{\equal{#1}{toc}}{\addchap{Confidentiality clause}}{\addchap*{Confidentiality clause}}
	%\section*{Confidentiality clause}
	\vspace*{2em}
	The {\Arbeit} on hand {\itshape{} \Title{}\/}contains internal resp.\ confidential data of {\Firma}. It is intended solely for inspection by the assigned examiner, the head of the {\Studiengang} department and, if necessary, the Audit Committee \Hochschule. It is strictly forbidden
	\begin{itemize}
		\item to distribute the content of this paper (including data, figures, tables, charts etc.) as a whole or in extracts,
		\item to make copies or transcripts of this paper or of parts of it,
		\item to display this paper or make it available in digital, electronic or virtual form.
	\end{itemize}
	Exceptional cases may be considered through permission granted in written form by the author and {\Firma}.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}

%%%%%%%%%%%% List of tables %%%%%%
\let\oldlistoftables\listoftables
\renewcommand{\listoftables}[1][toc]{
	\cleardoublepage
	\ifthenelse{\equal{#1}{toc}}{\addcontentsline{toc}{chapter}{Tabellenverzeichnis}}{}
	\oldlistoftables
	\clearpage
}

%%%%%%%%% List of figures %%%%
\let\oldlistoffigures\listoffigures
\renewcommand{\listoffigures}[1][toc]{
	\cleardoublepage
	\ifthenelse{\equal{#1}{toc}}{\addcontentsline{toc}{chapter}{Abbildungsverzeichnis}}{}
	\oldlistoffigures
	\clearpage
}
%%%%%%%%%% Abstract %%%%%%%%%%
\newcommand{\abstractDE}[2][toc]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{Zusammenfassung}}{\addchap*{Zusammenfassung}}
	#2
	\clearpage
}
\newcommand{\abstractEN}[2][toc]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{Abstract}}{\addchap*{Abstract}}
	\begin{otherlanguage}{english}
		#2
	\end{otherlanguage}
	\clearpage
}
%%%%%%%%%% Table of content %%%%%%%%%%%
\let\oldtableofcontents\tableofcontents
\renewcommand{\tableofcontents}{
	\clearpage
	\begin{spacing}{1.0}
		\begingroup

		% auskommentieren für Seitenzahlen unter Inhaltsverzeichnis
		\renewcommand*{\chapterpagestyle}{empty}
		\pagestyle{empty}


		\setcounter{tocdepth}{2}
		%für die Anzeige von Unterkapiteln im Inhaltsverzeichnis
		%\setcounter{tocdepth}{2}

		\oldtableofcontents
		\clearpage
		\endgroup
	\end{spacing}
	%	\pagenumbering{gobble}
	%	\pagestyle{empty}
	%	\setcounter{tocdepth}{2}
	%	\oldtableofcontents
	\clearpage
	\textStyle
	%\setboolean{alreadyEnded}{true}
}
%%%%%%%%%% Past all Content %%%%%%%%%
\newcommand{\content}[2]{
	\textNumbering
	\foreach \i in {01,02,03,04,05,06,07,08,09,...,99} {
		\edef\FileName{#1/\i #2}
			\IfFileExists{\FileName}{
				\input{\FileName}
			}
			{
				%file does not exist
			}
		}
		\clearpage
		\headNumbering

}
\renewcommand*{\chapterheadstartvskip}{\vspace*{.5\baselineskip}}% Abstand einstellen
%%%%%%%%%% Einstellungen %%%%%%%%%%
% Hurenkinder und Schusterjungen verhindern
% http://projekte.dante.de/DanteFAQ/Silbentrennung
\clubpenalty = 10000 % schließt Schusterjungen aus (Seitenumbruch nach der ersten Zeile eines neuen Absatzes)
\widowpenalty = 10000 % schließt Hurenkinder aus (die letzte Zeile eines Absatzes steht auf einer neuen Seite)
\displaywidowpenalty=10000
\setstretch{1,5}
\headStyle
\headNumbering
\defbibheading{head}{\addchap{Literaturverzeichnis}}
